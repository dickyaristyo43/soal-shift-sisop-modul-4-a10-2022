#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <wait.h>
#include <sys/stat.h>
#include <ctype.h>
#include <time.h>

static  const  char *dirpath = "/home/andrianalif/Documents";

char *anime = "Animeku_";
char *ian = "IAN_";
char *key = "INNUGANTENG";

// Mengembalikan index posisi titik 
int findExtension(char *path)
{
    int len = strlen(path);
    for(int i = len-1; i >= 0; i--)
    {
        if(path[i] == '.')
        {
            return i;
        }
    }
    return len;
}

// Mengembalikan index posisi slash
int findSlash(char *path, int offset)
{
    int len = strlen(path);
    for(int i = 0; i < len; i++)
    {
        if(path[i] == '/')
        {
            return i + 1;
        }
    }
    return offset;
}

// Menulis log
void writeLog(char *str, char *from, char *to, int mode)
{
    FILE *fptr;
	fptr = fopen("/home/andrianalif/modul4/Wibu.log", "a+");
    if(mode == 1)
        fprintf(fptr, "%s %s\n", str, from);
    else if(mode == 2)
        fprintf(fptr, "%s %s --> %s\n", str, from, to);
    fclose(fptr);
}

void encodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;
    
    int end = findExtension(path);
    int begin = findSlash(path, 0);
    
    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A'; 
            else continue;
            
            temp = 25 - temp; 
            
            if(isupper(path[i]))
                temp += 'A';

            path[i] = temp;
        }
    }
}

void decodeAtbash(char *path)
{
    if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, end);
    
    for (int i = begin; i < end; i++){
        if (isalpha(path[i])){
            char temp = path[i];
            if(isupper(path[i]))
                temp -= 'A';
            else continue;
            
            temp = 25 - temp;
            
            if(isupper(path[i]))
                temp += 'A';
            
            path[i] = temp;
        }
    }
}

void encodeRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, 0);

	for (int i = begin; i < end; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp = path[i];
			if(isupper(path[i])) 
                continue; 
            else
                temp -= 'a';
            
            temp = (temp + 13) % 26;
			
            if(isupper(path[i]))
                continue;
            else temp += 'a';
			
            path[i] = temp;
		}
	}
}

void decodeRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;

    int end = findExtension(path);
    int begin = findSlash(path, end);
	
	for (int i = begin; i < end; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp = path[i];
			if(isupper(path[i]))
                continue;
            else temp -= 'a';
            
            temp = (temp + 13) % 26;
			
            if(isupper(path[i])) continue;
            else 
                temp += 'a';
			
            path[i] = temp;
		}
	}
}

void encodeVignere(char *path){
	if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
	
	int end = strlen(path);
	int begin = findSlash(path, 0);

	for (int i = begin; i < end; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i]; 
            char key_temp = key[(i-begin) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'A';
            }
			else
            { 
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp + key_temp) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}
}

void decodeVignere(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) 
        return;
	
	int end = strlen(path);
	int begin = findSlash(path, end);
	
	for (int i = begin; i < end; i++){
		if (isalpha(path[i])){
			char msg_temp = path[i];
			char key_temp = key[(i-begin) % strlen(key)];
			if(isupper(path[i]))
            {
                msg_temp -= 'A'; 
                key_temp -= 'A';
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'A';
            }
			else
            {
                msg_temp -= 'a'; 
                key_temp = tolower(key_temp) - 'a'; 
                msg_temp = (msg_temp - key_temp + 26) % 26; 
                msg_temp += 'a';
            }
			path[i] = msg_temp;
		}
	}
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeRot13(satu);
        decodeAtbash(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (satu != NULL) {
            encodeAtbash(de->d_name);
            encodeRot13(de->d_name);
        }

        if (dua != NULL) encodeVignere(de->d_name);

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}


static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
	int res;

    char fpath[1024];

    char *satu = strstr(path, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(path, ian);
    if (dua != NULL) decodeVignere(dua);

    if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

	res = mkdir(path, mode);

    if(satu != NULL) writeLog("MKDIR terenkripsi", fpath, "", 1);
    else writeLog("MKDIR", fpath, "", 1);

	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromDir[1024], toDir[1024];

    char *satu = strstr(to, anime);
    if (satu != NULL) {
        decodeAtbash(satu);
        decodeRot13(satu);
    }

    char *dua = strstr(to, ian);
    if (dua != NULL) decodeVignere(dua);

    sprintf(fromDir, "%s%s", dirpath, from);
    sprintf(toDir, "%s%s", dirpath, to);

    if(strstr(toDir, anime) != NULL) writeLog("RENAME terenkripsi", fromDir, toDir, 2);
    else if(strstr(fromDir, anime) != NULL) writeLog("RENAME terdecode", fromDir, toDir, 2);

	res = rename(fromDir, toDir);
	if (res == -1)
		return -errno;

	return 0;
}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
};



int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

/* Referensi
 * https://www.cs.hmc.edu/~geoff/classes/hmc.cs135.201109/homework/fuse/fusexmp.c 
 * https://www.base64encode.org/
 * https://rot13.com/
 * youtube.com/watch?v=b1SmB2Myfxk
 * https://codereview.stackexchange.com/questions/81886/rot13-algorithm-in-c
 * https://github.com/randerson112358/C-Programs/blob/master/vigenereCipher.c
 * https://www.programmingalgorithms.com/algorithm/vigenere-cipher/c/ 
 * https://www.dcode.fr/vigenere-cipher
 */

}
